from flask import Flask, render_template

from database.workout_db import WorkoutDatabase

app = Flask(__name__)
db = WorkoutDatabase()


@app.route('/', methods=['GET'])
def index():
    data = db.read(None)
    return render_template('index.html', data=data)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
