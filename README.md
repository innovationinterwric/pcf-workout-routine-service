# MICROSERVICE pcf-workout-routine-service

This repository has all information about workout routines expose as html.

Dependencies:
-
- Python 3.6.8
- flask
- pymongo

Using
- Docker
- Pivotal Cloud Foundry

Deploy
-
This project has configured a Jenkins file for build and deploy. Use pivotal cloud foundry
Create an account in [Pivotal Cloud Foundry](https://pivotal.io/platform)

Commands
-
docker build -t riciw1050/pcf-workout-routine-service:1.0.0 .
docker push riciw1050/pcf-workout-routine-service:1.0.0