FROM python:3-alpine

ENV WORKOUT_DB_HOST us-cdbr-iron-east-02.cleardb.net
ENV USER_DB b3d0c9cd08f961
ENV PASSWORD f08679ba
ENV WORKOUT_SCHEMA ad_178f1f5c23b2f33

ADD ./templates/index.html /templates/index.html
ADD ./app.py /app.py
ADD ./database/workout_db.py /database/workout_db.py
ADD ./requirements.txt /requirements.txt

EXPOSE 5000

RUN pip install -r requirements.txt

CMD ["python", "app.py"]