import os

import pymysql


def connect():
    return pymysql.connect(host=os.environ["WORKOUT_DB_HOST"],
                           user=os.environ["USER_DB"],
                           password=os.environ["PASSWORD"],
                           db=os.environ["WORKOUT_SCHEMA"])


class WorkoutDatabase:

    def read(self, id):
        conn = connect()
        cursor = conn.cursor()

        try:
            cursor.execute("SELECT * FROM program")
            return cursor.fetchall()
        except:
            return ()
        finally:
            conn.close()
